<%@page import="java.util.Locale"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <% 
            Date hoy = new Date();  
            DateFormat format = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale("es", "CR"));
        %>
        <h1>La fecha de hoy es: <%=format.format(hoy)%></h1>
    </body>
</html>
