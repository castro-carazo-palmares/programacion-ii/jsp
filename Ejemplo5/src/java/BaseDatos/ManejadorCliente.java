package BaseDatos;

import Modelo.Cliente;
import java.sql.*;
import java.util.*;

public class ManejadorCliente {

    private final String SQL_SELECT = "SELECT id_cliente, nombre, apellido, email, telefono, saldo "
            + " FROM cliente";

    private final String SQL_SELECT_BY_ID = "SELECT id_cliente, nombre, apellido, email, telefono, saldo "
            + " FROM cliente WHERE id_cliente = ?";

    private final String SQL_INSERT = "INSERT INTO cliente(nombre, apellido, email, telefono, saldo) "
            + " VALUES(?, ?, ?, ?, ?)";

    private final String SQL_UPDATE = "UPDATE cliente "
            + " SET nombre=?, apellido=?, email=?, telefono=?, saldo=? WHERE id_cliente=?";

    private final String SQL_DELETE = "DELETE FROM cliente WHERE id_cliente = ?";

    Conexion conexion;

    public ManejadorCliente() {
        conexion = new Conexion();
    }

    public List<Cliente> listar() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Cliente cliente = null;
        List<Cliente> clientes = new ArrayList<>();
        
        try {
            conexion.openDB();
            connection = conexion.getConnection();
            statement = connection.prepareStatement(SQL_SELECT);
            resultSet = statement.executeQuery();
            
            while (resultSet.next()) {
                int idCliente = resultSet.getInt("id_cliente");
                String nombre = resultSet.getString("nombre");
                String apellido = resultSet.getString("apellido");
                String email = resultSet.getString("email");
                String telefono = resultSet.getString("telefono");
                double saldo = resultSet.getDouble("saldo");

                cliente = new Cliente(idCliente, nombre, apellido, email, telefono, saldo);
                clientes.add(cliente);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            conexion.closeDB();
        }
        return clientes;
    }

    public Cliente encontrar(Cliente cliente) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            conexion.openDB();
            connection = conexion.getConnection();
            statement = connection.prepareStatement(SQL_SELECT_BY_ID);
            statement.setInt(1, cliente.getIdCliente());
            
            resultSet = statement.executeQuery();
            resultSet.absolute(1);//Nos posicionamos en el primer registro devuelto por la base de datos.

            String nombre = resultSet.getString("nombre");
            String apellido = resultSet.getString("apellido");
            String email = resultSet.getString("email");
            String telefono = resultSet.getString("telefono");
            double saldo = resultSet.getDouble("saldo");

            cliente.setNombre(nombre);
            cliente.setApellido(apellido);
            cliente.setEmail(email);
            cliente.setTelefono(telefono);
            cliente.setSaldo(saldo);

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            conexion.closeDB();
        }
        return cliente;
    }

    public int insertar(Cliente cliente) {
        Connection connection = null;
        PreparedStatement statement = null;
        int rows = 0;
        
        try {
            conexion.openDB();
            connection = conexion.getConnection();
            statement = connection.prepareStatement(SQL_INSERT);
            statement.setString(1, cliente.getNombre());
            statement.setString(2, cliente.getApellido());
            statement.setString(3, cliente.getEmail());
            statement.setString(4, cliente.getTelefono());
            statement.setDouble(5, cliente.getSaldo());

            rows = statement.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            conexion.closeDB();
        }
        
        return rows;
    }

    public int actualizar(Cliente cliente) {
        
        Connection connection = null;
        PreparedStatement statement = null;
        int rows = 0;
        
        try {
            
            conexion.openDB();
            connection = conexion.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, cliente.getNombre());
            statement.setString(2, cliente.getApellido());
            statement.setString(3, cliente.getEmail());
            statement.setString(4, cliente.getTelefono());
            statement.setDouble(5, cliente.getSaldo());
            statement.setInt(6, cliente.getIdCliente());

            rows = statement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            conexion.closeDB();
        }
        
        return rows;
    }

    public int eliminar(Cliente cliente) {
        
        Connection connection = null;
        PreparedStatement statement = null;
        int rows = 0;
        
        try {
            conexion.openDB();
            connection = conexion.getConnection();
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setInt(1, cliente.getIdCliente());

            rows = statement.executeUpdate();
            
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            conexion.closeDB();
        }
        
        return rows;
    }

}
