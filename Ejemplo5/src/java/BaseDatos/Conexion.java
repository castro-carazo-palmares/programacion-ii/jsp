package BaseDatos;

import java.sql.*;

public class Conexion {

    private final String host = "localhost:3306";//Contiene el host de la BD.
    private final String database = "control_clientes";//Contiene el nombre de la BD.
    private final String url = "jdbc:mysql://" + host + "/" + database + "?autoReconnect=true&useSSL=false";//Contiene la dirección de la BD.
    private final String USER = "root";//Contiene el usuario de la BD.
    private final String PASS = "rootroot";//Contiene el password de la BD.
    private final String DRIVER = "com.mysql.jdbc.Driver";//Contiene la dirección del Driver de MySQL.
    Connection connection = null;//Inicializa la variable conexión a la BD.

    /**
     * Abre la Base de Datos.
     */
    public void openDB() {

        try {

            Class.forName(DRIVER);
            connection = (Connection) DriverManager.getConnection(url, USER, PASS);//Crea la conexión a la BD.

        }//Fin de try
        catch (Exception ex) {

            System.err.println("Hubo un error abriendo la Base de Datos");

        }//Fin de catch

    }//Fin de clase openDB

    public Connection getConnection(){
        return this.connection;
    }

    /**
     * Cierra la Base de Datos.
     */
    public void closeDB() {

        try {
            connection.close();//Termina la conxión con la BD.
        } //Fin de try
        catch (SQLException ex) {

            System.err.println("Hubo un error al cerrar la Base de Datos");

        }//Fin de catch

    }//Fin del método closeDB
}
