<%@page import="logica.Tabla"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EjemploJSP</title>
    </head>
    <body>
        <%
            HttpSession s = request.getSession();
            
            String nombre = (String)s.getAttribute("nombre");
            int max = -1;
            if(!"".equals((String)s.getAttribute("numero"))){
                max = Integer.parseInt((String)s.getAttribute("numero"));
            }
        %>
        <%
            if(!"".equals(nombre)){
                
            
        %>
        <h1>Hola <%=nombre%>!</h1>
        
        <%
   
            } else {
        %>
        <h1>Hola!</h1>
        <%
   
            } 
        %>
        <h2>Prueba bot&oacute;n Enviar 1!</h2>
        <%
            if (max < 0 || max > 20) { //Hasta 20 para que quepa en la pantalla
                
        %>
        <h3>El número mínimo es 0 y el máximo 20</h3>
        
        <%
            } else {
        %>
        <h3>El número recibido es <%=max%></h3>
         <%--Aquí imprimir la tabla de multiplicar desde 0 hasta el número recibido,
        utilizando ciclos para que sea dinámico--%> 
        <%      
            }

            Tabla tabla = new Tabla(max);
            String[] vector = tabla.calcTabla();
            
            for(int i = 0; i < 10; i++) {
        %>
                
                <p style="margin-left: 40px"><%=vector[i]%></p>
        <%
            }
        %>
     
    </body>
</html>
