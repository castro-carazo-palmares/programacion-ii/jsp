package beans;

public class Persona {
    private String nombre;
    private int edad;
    private char sexo;

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
    
    public int dobleEdad(){
        return edad * 2;
    }

    @Override
    public String toString() {
        return "";
    }
    
    
}
