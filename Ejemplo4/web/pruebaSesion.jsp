
<%@page import="beans.Persona"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PruebaSesion</title>
    </head>
    <body>
        <h1>Prueba permanencia del bean en la sesi�n</h1>
        <jsp:useBean id="persona" scope="session" class="beans.Persona" />
        <%
            //HttpSession s = request.getSession();
            //Persona persona2 = (Persona)s.getAttribute("p2");
            //out.print("entr�");
        
        %>
        
        <h2>Datos de la persona:</h2>
        <p>Nombre: <jsp:getProperty name="persona" property="nombre" /></p>
        <p>Edad: <jsp:getProperty name="persona" property="edad" /></p>
        <p>Sexo: <jsp:getProperty name="persona" property="sexo" /></p>         
               
    </body>
</html>
