<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="beans.Persona"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EjUsoBean</title>
    </head>
    <body>
        <h1>Ejemplo uso del bean</h1>
        <%--
        useBean: para instanciar el objeto
        id: para identificar el objeto en el jsp
        scope: ambito del objeto, ver otras opciones
        class: ubicación en el src
        --%>
        <jsp:useBean id="persona" scope="session" class="beans.Persona" />
         <%--la siguiente forma se puede si no debemos procesar el parámetro--%>
        <jsp:setProperty name="persona" property="nombre" param="nombre" />
        <%--la siguiente forma se hace si debemos procesar el parámetro--%>
        <%
            int edad = 0;
            if(!"".equals(request.getParameter("edad"))){
                edad = Integer.parseInt(request.getParameter("edad"));
            }
        %>
        <jsp:setProperty name="persona" property="edad" value="<%=edad%>" />
        <%
            char sexo = 'F';
            if(request.getParameter("sexo").equals("m")){
                sexo = 'M';
            }
            
            //Persona p2 = new Persona("Jose", edad, sexo);
            //HttpSession s = request.getSession();
            //s.setAttribute("newp", p2);
        %>
        <jsp:setProperty name="persona" property="sexo" value="<%=sexo%>" />
        <h2>Datos de la persona:</h2>
        <p>Nombre: <jsp:getProperty name="persona" property="nombre" /></p>
        <p>Edad: <jsp:getProperty name="persona" property="edad" /></p>
        <p>Sexo: <jsp:getProperty name="persona" property="sexo" /></p>
        
        <p>Doble de la edad: <%=persona.dobleEdad()%></p> 
        <a href="pruebaSesion.jsp"><p>Prueba permanencia del bean en la sesión</p></a>
    </body>
</html>
