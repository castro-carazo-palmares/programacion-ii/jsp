<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Página3</title>
        <link rel="stylesheet" type="text/css" href="mystyles.css">
    </head>
    <body>
        <div id="container">
            <%@include file="header.html" %>          
            <div id="menu">
                <b>Menu</b><br/>
                HTML<br/>
                CSS<br/>
                JavaScript
            </div>
            <div id="content">
                Content goes here
            </div>
            <%@include file="footer.html" %>
        </div>
    </body>
</html>
